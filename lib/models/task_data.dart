import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:todoey/models/task.dart';
import 'package:todoey/shared/sql_controller.dart';

class TaskData extends ChangeNotifier {
  List<Task> _tasksOld = [
    Task(name: 'Buy Milk', id: -1),
    Task(name: 'Buy Eggs', id: -1),
    Task(name: 'Buy Bread', id: -1),
  ];
  List<Task> _tasks = [];

  UnmodifiableListView<Task> get tasks {
    return UnmodifiableListView(_tasks);
  }

  int get taskCount {
    return _tasks.length;
  }

  void addTask(String newTaskTitle) {
    final task = Task(name: newTaskTitle);
    _tasks.add(task);
    notifyListeners();
  }

  void deleteTask(Task task) {
    _tasks.remove(task);
    SqlController.delete(task.id);
    notifyListeners();
  }

  void getDataSQL() {
    SqlController.getAllData().then((value) {
      _tasks = [];
      for (int i = 0; i < value.length; i++) {
        bool done = value[i]['isDone'] == 0 ? false : true;
        _tasks.add(
            Task(name: value[i]['name'], isDone: done, id: value[i]['id']));
      }
      print('get from DB : ${_tasks.length}');
      notifyListeners();
    });
  }

  void addTaskToDB(name) {
    SqlController.insertTaks(Task(name: name)).then((value) {
      getDataSQL();
    });
  }

  void updateTaskStatus(Task task) {
    task.toggleDone();
    SqlController.updateStatus(task.id, task.isDone);
    notifyListeners();
  }

  void updateTaskName(Task? task) {
    if (task != null) {
      SqlController.updateName(task.id, task.name);
      notifyListeners();
    }
  }
}
