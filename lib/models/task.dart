class Task {
  String name;
  bool isDone;
  int id;

  Task({required this.name, this.isDone = false, this.id = -1});

  void toggleDone() {
    isDone = !isDone;
  }

  void changeName(String newName) {
    name = newName;
  }
}
