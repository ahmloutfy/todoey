import 'package:flutter/material.dart';

class TaskTile extends StatelessWidget {
  final bool isChecked;
  final String taskTitle;
  final Function checkboxCallback;
  final void Function() longPressCallback;

  TaskTile(
      {required this.isChecked,
      required this.taskTitle,
      required this.checkboxCallback,
      required this.longPressCallback});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: longPressCallback,
      title: Text(
        taskTitle,
        style: TextStyle(
            decoration: isChecked ? TextDecoration.lineThrough : null,
            fontWeight: FontWeight.bold,
            fontSize: 18.0),
      ),
      trailing: Checkbox(
        activeColor: Colors.lightBlueAccent,
        side: BorderSide(
          width: 3.0,
        ),
        value: isChecked,
        onChanged: checkboxCallback as void Function(bool?),
      ),
    );
  }
}
