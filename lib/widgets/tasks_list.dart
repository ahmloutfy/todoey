import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey/models/task_data.dart';

import '../models/task.dart';
import '../screens/add_task_screen.dart';
import 'tasks_tile.dart';

class TasksList extends StatelessWidget {
  final bool _isShown = true;

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskData>(
      builder: (context, taskData, child) {
        return ListView.builder(
          itemBuilder: (context, index) {
            final task = taskData.tasks[index];
            return TaskTile(
              taskTitle: task.name,
              isChecked: task.isDone,
              checkboxCallback: (checkboxState) {
                taskData.updateTaskStatus(task);
              },
              longPressCallback: () {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text("What to do?"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("Cancel"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text("Edit"),
                          onPressed: () => showModelBottomEdit(context, task),
                        ),
                        FlatButton(
                          child: Text("Delete"),
                          onPressed: () {
                            taskData.deleteTask(task);
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  },
                );
              },
            );
          },
          itemCount: taskData.taskCount,
          itemExtent: 35,
        );
      },
    );
  }

  void showModelBottomEdit(BuildContext context, Task task) {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
    showModalBottomSheet(
        context: context,
        builder: (context) => AddTaskScreen(
              izAdd: false,
              newTask: task,
            ));
  }
}
