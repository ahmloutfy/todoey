import 'package:sqflite/sqflite.dart';
import 'package:todoey/models/task.dart';

class SqlController {
  static String _table = 'tasks';
  static String _DBName = 'todo.db';
  static late Database _database;

  static Future<int> openOrCreatDB() async {
    String cT =
        'CREATE TABLE $_table (id INTEGER PRIMARY KEY, name TEXT, isDone BOOLEAN)';
    await openDatabase(_DBName, version: 1, onCreate: (db, version) {
      print('db created');
      _database = db;
      db.execute(cT).then((value) {
        print('table created');
      }).catchError((error) {
        print('ERROR when creating table ${error.toString()}');
      });
    }, onOpen: (db) {
      print('db opened');
      _database = db;
    });
    return 0;
  }

  static Future<int> insertTaks(Task task) async {
    String command =
        'INSERT INTO $_table(name, isDone) VALUES("${task.name}", ${task.isDone})';
    print('command : $command');
    await _database.transaction((txn) async {
      return await txn.rawInsert(command);
    });
    return -1;
  }

  static Future<List<Map>> getAllData() async {
    return await _database.rawQuery('SELECT * FROM $_table');
  }

  static Future<int> updateStatus(int id, izDone) async {
    String izDoneAsSring = izDone ? '1' : '0';
    return await _database.rawUpdate(
        'UPDATE $_table SET isDone = ? WHERE id = ?', [izDoneAsSring, id]);
  }

  static Future<int> updateName(int id, String newName) async {
    return await _database
        .rawUpdate('UPDATE $_table SET name = ? WHERE id = ?', [newName, id]);
  }

  static Future<int> delete(int id) async {
    return await _database.delete(
      _table, // replace with table name
      where: "id = ?",
      whereArgs: [id], // you need the id
    );
  }
}
