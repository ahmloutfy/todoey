import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey/models/task.dart';
import 'package:todoey/models/task_data.dart';
import 'package:todoey/screens/add_task_screen.dart';
import 'package:todoey/shared/sql_controller.dart';
import 'package:todoey/widgets/tasks_list.dart';

class TasksScreen extends StatelessWidget {
  bool izFirst = true;
  @override
  Widget build(BuildContext context) {
    myContext = context;
    if (izFirst) {
      print('getting data from DB');
      Provider.of<TaskData>(context).getDataSQL();
      izFirst = false;
    }
    return Scaffold(
      floatingActionButton: myFAB(),
      backgroundColor: Colors.lightBlueAccent,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
                top: 60.0, left: 30.0, right: 30.0, bottom: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.list,
                    size: 30.0,
                    color: Colors.lightBlueAccent,
                  ),
                  radius: 30.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  'Todoey',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 50.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  '${Provider.of<TaskData>(context).taskCount} Tasks',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
              ),
              child: TasksList(),
            ),
          ),
        ],
      ),
    );
  }

  // vars
  late BuildContext myContext;

  FloatingActionButton myFAB() => FloatingActionButton(
        backgroundColor: Colors.lightBlueAccent,
        // onPressed: () =>
        //     Provider.of<TaskData>(context, listen: false).getDataSQL(),
        onPressed: () => showModelBottom(myContext),
        // onPressed: callAll(),
        child: Icon(Icons.add),
      );

  void showModelBottom(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) => AddTaskScreen(
              izAdd: true,
            ));
  }

  void testInsert() {
    SqlController.insertTaks(new Task(name: 'name', isDone: false))
        .then((value) => testGetAll());
  }

  void testGetAll() {
    SqlController.getAllData().then((value) {
      print(value[1]);
      print(value[1]['name']);
    });
  }

  void testUpdate() {
    SqlController.updateStatus(2, false);
  }

  VoidCallback callAll() => () {
        print('hi');
        testUpdate();
        testGetAll();
      };
}
