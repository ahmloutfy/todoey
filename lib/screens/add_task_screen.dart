import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoey/models/task.dart';
import 'package:todoey/models/task_data.dart';

TextEditingController nameController = TextEditingController();

class AddTaskScreen extends StatelessWidget {
  bool izAdd;

  Task? newTask;

  AddTaskScreen({this.izAdd = true, this.newTask}) {
    nameController.text = newTask == null ? '' : newTask!.name;
  }

  late BuildContext myContext;

  @override
  Widget build(BuildContext context) {
    myContext = context;
    return Container(
      color: Color(0xFF757575),
      child: Container(
        padding: EdgeInsets.fromLTRB(60, 30, 60, 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              izAdd ? 'Add Task' : 'Edit Task',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.lightBlueAccent, fontSize: 30.0),
            ),
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.lightBlueAccent, width: 5.0),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.lightBlueAccent, width: 5.0),
                ),
                errorBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.lightBlueAccent, width: 5.0),
                ),
                focusedErrorBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.lightBlueAccent, width: 5.0),
                ),
              ),
              autofocus: true,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 30.0,
            ),
            FlatButton(
              height: 50.0,
              onPressed: izAdd ? addTask() : editTask(),
              child: Text(
                izAdd ? 'Add' : 'edit',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              color: Colors.lightBlueAccent,
            ),
          ],
        ),
      ),
    );
  }

  VoidCallback addTask() => () {
        Provider.of<TaskData>(myContext, listen: false)
            .addTaskToDB(nameController.text);
        Navigator.pop(myContext);
      };

  VoidCallback editTask() => () {
        newTask!.changeName(nameController.text);
        Provider.of<TaskData>(myContext, listen: false).updateTaskName(newTask);
        Navigator.pop(myContext);
      };
}
